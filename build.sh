#!/bin/bash
# https://gitlab.com/nixtux-packaging/nixtux-repo

RPM_source_dir="${HOME}/RPM/SOURCES"

if [ ! -d "$RPM_source_dir" ]; then
	mkdir -p "$RPM_source_dir"
fi

for i in nixtux-keys.list nixtux-repo.list mikhailnov_pub.gpg nixtux-keys.filetrigger
do
	rm -fv "$RPM_source_dir/$i"
	cp -v "$i" "$RPM_source_dir/"
done

# -ba build src.rpm and rpm
rpmbuild -ba nixtux-repo.spec

# gpg --with-fingerprint mikhailnov_pub.gpg

